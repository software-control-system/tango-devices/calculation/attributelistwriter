package fr.soleil.tango.server.attributelistwriter.dataaccess;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.clientapi.TangoGroupAttribute;

public class AsyncGroupWriter {
	private static final int PERIOD_POLLING_PRE_POST_WRITE = 100;

	private final Logger logger = LoggerFactory.getLogger(AsyncGroupWriter.class);

	private final ExecutorService executor = Executors.newSingleThreadExecutor();

	private final AsyncTask ce;

	private final AuditTrailer auditTrailer;

	private final String writerName;

	private class AsyncTask implements Callable<Void> {

		private final Object[] values;
		private final String stringValues;
		private final TangoGroupAttribute group;

		private final Object[] valuesPreWriting;
		private final String stringValuesPreWriting;
		private final AsyncGroupReader asyncGroupReaderPreWriting;
		private final Map<String, Pair<String, Double>> valuesAccuraciesMapPreWriting;
		private final TangoGroupAttribute groupPreWriting;

		private final Object[] valuesPostWriting;
		private final String stringValuesPostWriting;
		private final AsyncGroupReader asyncGroupReaderPostWriting;
		private final Map<String, Pair<String, Double>> valuesAccuraciesMapPostWriting;
		private final TangoGroupAttribute groupPostWriting;

		/**
		 * 
		 * @param configList            A triple representing, in the order: attribute /
		 *                              value / accuracy. The accuracy is unused here.
		 * @param preWritingTripleList  A pair representing, in the order : attribute /
		 *                              value / accuracy
		 * @param postWritingTripleList A pair representing, in the order : attribute /
		 *                              value / accuracy
		 * @throws DevFailed A <code>DevFailed</code> exception is thrown if the
		 *                   connection to an attribute is impossible.
		 */
		public AsyncTask(final List<Triple<String, String, Double>> configList,
				final List<Triple<String, String, Double>> preWritingTripleList,
				final List<Triple<String, String, Double>> postWritingTripleList) throws DevFailed {
			values = new Object[configList.size()];
			final String[] attributeNames = new String[configList.size()];
			for (int i = 0; i < configList.size(); i++) {
				final Triple<String, String, Double> triple = configList.get(i);
				attributeNames[i] = triple.getLeft();
				values[i] = triple.getMiddle();
			}
			group = new TangoGroupAttribute(attributeNames);
			stringValues = " - " + Arrays.toString(values);

			if (preWritingTripleList != null && !preWritingTripleList.isEmpty()) {
				valuesAccuraciesMapPreWriting = new HashMap<>(preWritingTripleList.size());
				valuesPreWriting = new Object[preWritingTripleList.size()];
				final String[] attributeNamesPreWriting = new String[preWritingTripleList.size()];
				for (int i = 0; i < preWritingTripleList.size(); i++) {
					final Triple<String, String, Double> triple = preWritingTripleList.get(i);
					attributeNamesPreWriting[i] = triple.getLeft();
					valuesPreWriting[i] = triple.getMiddle();
					valuesAccuraciesMapPreWriting.put(triple.getLeft(),
							new ImmutablePair<String, Double>(triple.getMiddle(), triple.getRight()));
				}
				groupPreWriting = new TangoGroupAttribute(attributeNamesPreWriting);
				stringValuesPreWriting = " - " + Arrays.toString(valuesPreWriting);
				asyncGroupReaderPreWriting = new AsyncGroupReader(auditTrailer, Arrays.asList(attributeNamesPreWriting),
						PERIOD_POLLING_PRE_POST_WRITE);
			} else {
				valuesPreWriting = null;
				stringValuesPreWriting = null;
				groupPreWriting = null;
				valuesAccuraciesMapPreWriting = null;
				asyncGroupReaderPreWriting = null;
			}

			if (postWritingTripleList != null && !postWritingTripleList.isEmpty()) {
				valuesAccuraciesMapPostWriting = new HashMap<>(postWritingTripleList.size());
				valuesPostWriting = new Object[postWritingTripleList.size()];
				final String[] attributeNamesPostWriting = new String[postWritingTripleList.size()];
				for (int i = 0; i < postWritingTripleList.size(); i++) {
					final Triple<String, String, Double> triple = postWritingTripleList.get(i);
					attributeNamesPostWriting[i] = triple.getLeft();
					valuesPostWriting[i] = triple.getMiddle();
					valuesAccuraciesMapPostWriting.put(triple.getLeft(),
							new ImmutablePair<String, Double>(triple.getMiddle(), triple.getRight()));
				}
				groupPostWriting = new TangoGroupAttribute(attributeNamesPostWriting);
				stringValuesPostWriting = " - " + Arrays.toString(valuesPostWriting);
				asyncGroupReaderPostWriting = new AsyncGroupReader(auditTrailer,
						Arrays.asList(attributeNamesPostWriting), PERIOD_POLLING_PRE_POST_WRITE);
			} else {
				valuesPostWriting = null;
				stringValuesPostWriting = null;
				groupPostWriting = null;
				valuesAccuraciesMapPostWriting = null;
				asyncGroupReaderPostWriting = null;
			}
		}

		private void readUntilValueReached(AsyncGroupReader asyncGroupReader,
				Map<String, Pair<String, Double>> valuesAccuraciesMap) {
			boolean reached = false;
			while (!reached) {
				Map<String, String> valuesMap = asyncGroupReader.getCurrentValues();
				for (Map.Entry<String, String> entry : valuesMap.entrySet()) {
					final String attributeName = entry.getKey().toLowerCase();
					Pair<String, Double> valueAndAccuracyPair = valuesAccuraciesMap.get(attributeName);
					final String writeValue = valueAndAccuracyPair.getLeft();
					final double accuracy = valueAndAccuracyPair.getRight();
					logger.debug("read attribute name/writevalue/accuracy {}/{}", attributeName, valueAndAccuracyPair);
					final String currentValue = entry.getValue();
					logger.debug("read attribute {}, value = {}", attributeName, currentValue);
					if (currentValue != null) {
						try {
							// compare numbers
							final double value = Double.valueOf(currentValue);
							final double lowLimit = Double.valueOf(writeValue) - accuracy;
							final double hightLimit = Double.valueOf(writeValue) + accuracy;
							logger.debug("read attribute {}, low limit = {}", attributeName, lowLimit);
							logger.debug("read attribute {}, high limit = {}", attributeName, hightLimit);
							if (value >= lowLimit && value <= hightLimit) {
								reached = true;
							} else {
								reached = false;
								break;
							}
						} catch (final NumberFormatException e) {
							// string case, no accuracy
							//logger.info("read attribute value {}, expected = {}", currentValue, writeValue);
							if (currentValue.trim().toLowerCase().equals(writeValue.trim().toLowerCase())) {
								reached = true;
							}
						}
					}

				}
				if (!reached) {
					try {
						Thread.sleep(PERIOD_POLLING_PRE_POST_WRITE);
					} catch (InterruptedException e) {
						auditTrailer.notifyReadError(DevFailedUtils.newDevFailed(e));
					}
				}
			}
		}

		@Override
		public Void call() throws Exception {
			try {
				// Pre write action
				if (groupPreWriting != null) {
					if (groupPreWriting != null) {
						AsyncGroupWriter.this.auditTrailer
								.notifyEvent(AsyncGroupWriter.this.writerName + " pre write " + stringValuesPreWriting);
					}
					groupPreWriting.write(valuesPreWriting);
					readUntilValueReached(asyncGroupReaderPreWriting, valuesAccuraciesMapPreWriting);
				}

				AsyncGroupWriter.this.auditTrailer.notifyEvent(AsyncGroupWriter.this.writerName + stringValues);
				group.write(values);

				// Post write action
				if (groupPostWriting != null) {
					if (groupPostWriting != null) {
						AsyncGroupWriter.this.auditTrailer
								.notifyEvent(AsyncGroupWriter.this.writerName + " post write " + stringValuesPostWriting);
					}
					groupPostWriting.write(valuesPostWriting);
					readUntilValueReached(asyncGroupReaderPostWriting, valuesAccuraciesMapPostWriting);
				}
			} catch (final DevFailed e) {
				AsyncGroupWriter.this.auditTrailer.notifyWriteError(e);
				throw e;
			}
			return null;
		}
	}

	public AsyncGroupWriter(final AuditTrailer auditTrailer, final String writerName,
			final List<Triple<String, String, Double>> configList,
			final List<Triple<String, String, Double>> preWritingTripleList,
			final List<Triple<String, String, Double>> postWritingTripleList) throws DevFailed {
		this.auditTrailer = auditTrailer;
		ce = new AsyncTask(configList, preWritingTripleList, postWritingTripleList);
		this.writerName = writerName;
	}

	public void execute() throws DevFailed {
		executor.submit(ce);
	}

	public String getWriterName() {
		return writerName;
	}
}
