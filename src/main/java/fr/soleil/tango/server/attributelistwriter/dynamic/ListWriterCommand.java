package fr.soleil.tango.server.attributelistwriter.dynamic;

import org.tango.server.StateMachineBehavior;
import org.tango.server.command.CommandConfiguration;
import org.tango.server.command.ICommandBehavior;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.server.attributelistwriter.dataaccess.AsyncGroupWriter;

public final class ListWriterCommand implements ICommandBehavior {

    private final CommandConfiguration config = new CommandConfiguration();
    private final String name;
    private final AsyncGroupWriter asyncWriter;

    /**
     * Constructor to create configuration of dynamic command argout = argin = void
     *
     * @param String
     *            commandLabel
     * @param List
     *            <TangoAttribute> attributeList
     * @param String
     *            [] values
     * @throws DevFailed
     */
    public ListWriterCommand(final String commandLabel, final AsyncGroupWriter asyncWriter) throws DevFailed {
        name = commandLabel;
        config.setName(commandLabel);
        config.setInType(void.class);
        config.setOutType(void.class);
        this.asyncWriter = asyncWriter;
    }

    @Override
    public Object execute(final Object arg) throws DevFailed {
        asyncWriter.execute();
        return null;
    }

    @Override
    public CommandConfiguration getConfiguration() {
        return config;
    }

    public String getName() {
        return name;
    }

    @Override
    public StateMachineBehavior getStateMachine() {
        return null;
    }
}