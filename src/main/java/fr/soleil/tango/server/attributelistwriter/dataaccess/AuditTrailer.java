package fr.soleil.tango.server.attributelistwriter.dataaccess;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;

public final class AuditTrailer {
    private final Logger logger = LoggerFactory.getLogger(AuditTrailer.class);
    private final SimpleDateFormat simple_date_format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private final IAuditListener listener;

    public AuditTrailer(final IAuditListener listener) {
        this.listener = listener;
    }

    public void notifyEvent(final String eventName) {
        logger.info("writing {}", eventName);
        listener.setLastError("none");
        final String executedTimeStamp = simple_date_format.format(new Date());
        listener.setLastExecutedTimestamp(executedTimeStamp);
        listener.setLastExecutedEvent(eventName);
    }

    public void notifyWriteError(final DevFailed e) {
        logger.error("error writing: {}", DevFailedUtils.toString(e));
        String msg = e.getMessage();
        if (e.errors[0] != null) {
            msg = e.errors[0].desc + " - " + e.errors[0].reason;
        }
        listener.setLastError(msg);
    }

    public void notifyReadError(final DevFailed e) {
        logger.error("error reading: {}", DevFailedUtils.toString(e));
    }
}
