package fr.soleil.tango.server.attributelistwriter.dataaccess;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import fr.esrf.Tango.AttrQuality;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeInfoEx;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.soleil.tango.attributecomposer.AttributeGroupReader;
import fr.soleil.tango.attributecomposer.AttributeGroupScheduler;
import fr.soleil.tango.attributecomposer.IAttributeGroupTaskListener;
import fr.soleil.tango.clientapi.TangoGroupAttribute;

public class AsyncGroupReader implements IAttributeGroupTaskListener {

    // private final AttributeGroupReader reader;
    private final static int PERIOD = 3000;
    private final AuditTrailer auditTrailer;
    private final Map<String, String> currentValues = new LinkedHashMap<String, String>();

    public AsyncGroupReader(final AuditTrailer auditTrailer, final List<String> attributeList, int period) throws DevFailed {
        this.auditTrailer = auditTrailer;
        final AttributeGroupReader reader = new AttributeGroupReader(this, new TangoGroupAttribute(
                attributeList.toArray(new String[attributeList.size()])), false, true, false);
        final AttributeGroupScheduler scheduler = new AttributeGroupScheduler();
        scheduler.start(reader, period);
    }

    public AsyncGroupReader(final AuditTrailer auditTrailer, final List<String> attributeList) throws DevFailed {
    	this(auditTrailer, attributeList, PERIOD);
    }

    @Override
    public void updateDeviceAttribute(final DeviceAttribute[] resultGroup) {
    }

    @Override
    public void updateReadValue(final String completeAttributeName, final Object value) {
        currentValues.put(completeAttributeName, value.toString());
    }

    @Override
    public void updateErrorMessage(final String completeAttributeName, final String errorMessage) {
    }

    @Override
    public void updateWriteValue(final String completeAttributeName, final Object value) {

    }

    @Override
    public void updateWriteValueErrorMessage(final String completeAttributeName, final String errorMessage) {

    }

    @Override
    public void updateAttributeInfoEx(final String completeAttributeName, final AttributeInfoEx attributeInfo) {

    }

    @Override
    public void updateAttributeInfoErrorMessage(final String completeAttributeName, final String errorMessage) {

    }

    @Override
    public void updateQuality(final String completeAttributeName, final AttrQuality quality) {

    }

    @Override
    public void catchException(final Exception exception) {

    }

    @Override
    public void catchDevFailed(final DevFailed exception) {
        auditTrailer.notifyReadError(exception);
    }

    @Override
    public void readingLoopFinished() {

    }

    public Map<String, String> getCurrentValues() {
        return new LinkedHashMap<String, String>(currentValues);
    }
}
