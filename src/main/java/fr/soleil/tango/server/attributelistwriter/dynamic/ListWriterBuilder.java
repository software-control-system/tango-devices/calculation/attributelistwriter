package fr.soleil.tango.server.attributelistwriter.dynamic;

import java.util.List;

import org.apache.commons.lang3.tuple.Triple;
import org.tango.server.dynamic.DynamicManager;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.server.attributelistwriter.dataaccess.AsyncGroupReader;
import fr.soleil.tango.server.attributelistwriter.dataaccess.AsyncGroupWriter;
import fr.soleil.tango.server.attributelistwriter.dataaccess.AuditTrailer;
import fr.soleil.tango.server.attributelistwriter.dataaccess.IAuditListener;

public final class ListWriterBuilder {

    public static AsyncGroupReader buildListReader(final IAuditListener auditListener, final List<String> attributeList)
            throws DevFailed {
        final AuditTrailer trailer = new AuditTrailer(auditListener);
        return new AsyncGroupReader(trailer, attributeList);
    }

    public static ListWriterAttribute buildListWriter(final IAuditListener auditListener, final String writerName,
            final List<Triple<String, String, Double>> configList,
            final List<Triple<String, String, Double>> preWritingTripleList, 
            final List<Triple<String, String, Double>> postWritingTripleList, final DynamicManager dynMngt,
            final AsyncGroupReader reader) throws DevFailed {
        final AuditTrailer trailer = new AuditTrailer(auditListener);
        final AsyncGroupWriter writer = new AsyncGroupWriter(trailer, writerName, configList, preWritingTripleList, postWritingTripleList);
        final ListWriterCommand command = new ListWriterCommand(writerName, writer);
        dynMngt.addCommand(command);
        final ListWriterAttribute attribute = new ListWriterAttribute(writerName, writer, reader, configList);
        dynMngt.addAttribute(attribute);
        return attribute;
    }
}
