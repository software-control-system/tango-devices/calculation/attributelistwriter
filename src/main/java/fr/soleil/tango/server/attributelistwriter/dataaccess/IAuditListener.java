package fr.soleil.tango.server.attributelistwriter.dataaccess;

public interface IAuditListener {

    void setLastExecutedTimestamp(String timestamp);

    void setLastExecutedEvent(String eventName);

    void setLastError(String errorMsg);

}
