package fr.soleil.tango.server.attributelistwriter.dynamic;

import org.tango.server.StateMachineBehavior;
import org.tango.server.attribute.AttributeConfiguration;
import org.tango.server.attribute.AttributeValue;
import org.tango.server.attribute.IAttributeBehavior;

import fr.esrf.Tango.DevFailed;

public final class ListValuesAttribute implements IAttributeBehavior {

    private final AttributeConfiguration config;
    private final AttributeValue value = new AttributeValue();

    public ListValuesAttribute(final AttributeConfiguration config) {
        this.config = config;
    }

    /**
     * Constructor to create configuration of dynamic attribut
     *
     * @param name
     * @param attrValue
     * @throws DevFailed
     */
    public ListValuesAttribute(final String name, final String[] attrValue) throws DevFailed {
        config = new AttributeConfiguration();
        config.setName(name);
        config.setType(String[].class);
        value.setValue(attrValue);
    }

    @Override
    public AttributeConfiguration getConfiguration() {
        return config;
    }

    @Override
    public StateMachineBehavior getStateMachine() {
        return null;
    }

    @Override
    public AttributeValue getValue() throws DevFailed {
        return value;
    }

    @Override
    public void setValue(final AttributeValue arg0) throws DevFailed {
        // Not use : READ ONLY
    }

}