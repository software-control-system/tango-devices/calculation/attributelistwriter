package fr.soleil.tango.server.attributelistwriter.dynamic;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.server.StateMachineBehavior;
import org.tango.server.attribute.AttributeConfiguration;
import org.tango.server.attribute.AttributeValue;
import org.tango.server.attribute.IAttributeBehavior;

import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.server.attributelistwriter.dataaccess.AsyncGroupReader;
import fr.soleil.tango.server.attributelistwriter.dataaccess.AsyncGroupWriter;

public class ListWriterAttribute implements IAttributeBehavior {

    private final AsyncGroupWriter asyncWriter;
    private final AsyncGroupReader asyncReader;
    private final AttributeConfiguration config = new AttributeConfiguration();
    private final List<Triple<String, String, Double>> configList;
    private final Logger logger = LoggerFactory.getLogger(ListWriterAttribute.class);

    public ListWriterAttribute(final String writerName, final AsyncGroupWriter asyncWriter,
            final AsyncGroupReader asyncReader, final List<Triple<String, String, Double>> configList) throws DevFailed {
        this.asyncWriter = asyncWriter;
        this.asyncReader = asyncReader;
        this.configList = configList;
        config.setName(writerName);
        config.setType(boolean.class);
        config.setWritable(AttrWriteType.READ_WRITE);
    }

    public String getName() {
        return config.getName();
    }

    @Override
    public AttributeConfiguration getConfiguration() throws DevFailed {
        return config;
    }

    @Override
    public StateMachineBehavior getStateMachine() throws DevFailed {
        return null;
    }

    @Override
    public AttributeValue getValue() throws DevFailed {
        return new AttributeValue(isActive());
    }

    public boolean isActive() {
        logger.debug("read attribute {}", config.getName());
        // true if all values are reached +/- accuracy
        final Map<String, String> values = asyncReader.getCurrentValues();
        // compare values
        boolean isActive = false;
        logger.debug("values = {}", values);
        logger.debug("config = {}", configList);
        for (final Triple<String, String, Double> triple : configList) {
            final String attributeName = triple.getLeft();
            final String writeValue = triple.getMiddle();
            final double accuracy = triple.getRight();
            logger.debug("read attribute name/writevalue/accuracy {}", triple);
            final String currentValue = values.get(attributeName);
            logger.debug("read attribute {}, value = {}",attributeName, currentValue);
            if (currentValue != null) {
                try {
                    // compare numbers
                    final double value = Double.valueOf(currentValue);
                    final double lowLimit = Double.valueOf(writeValue) - accuracy;
                    final double hightLimit = Double.valueOf(writeValue) + accuracy;
                    logger.debug("read attribute {}, low limit = {}",attributeName, lowLimit);
                    logger.debug("read attribute {}, high limit = {}",attributeName, hightLimit);
                    if (value >= lowLimit && value <= hightLimit) {
                        isActive = true;
                    } else {
                        isActive = false;
                        break;
                    }
                } catch (final NumberFormatException e) {
                    // string case, no accuracy
                    if (currentValue.equals(triple.getMiddle())) {
                        isActive = true;
                    }
                }
            }
        }
        return isActive;
    }

    public void execute() throws DevFailed {
        asyncWriter.execute();
    }

    @Override
    public void setValue(final AttributeValue arg0) throws DevFailed {
        final boolean doApply = (Boolean) arg0.getValue();
        if (doApply) {
            execute();
        }

    }
}
