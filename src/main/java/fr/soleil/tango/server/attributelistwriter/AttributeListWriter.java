package fr.soleil.tango.server.attributelistwriter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.tango.DeviceState;
import org.tango.orb.ServerRequestInterceptor;
import org.tango.server.InvocationContext;
import org.tango.server.InvocationContext.ContextType;
import org.tango.server.ServerManager;
import org.tango.server.annotation.AroundInvoke;
import org.tango.server.annotation.Attribute;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DeviceManagement;
import org.tango.server.annotation.DeviceProperty;
import org.tango.server.annotation.DynamicManagement;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.Status;
import org.tango.server.annotation.TransactionType;
import org.tango.server.attribute.IAttributeBehavior;
import org.tango.server.attribute.log.LogAttribute;
import org.tango.server.device.DeviceManager;
import org.tango.server.dynamic.DynamicManager;
import org.tango.server.dynamic.command.AsyncGroupCommand;
import org.tango.utils.CircuitBreakerCommand;
import org.tango.utils.ClientIDUtil;
import org.tango.utils.DevFailedUtils;
import org.tango.utils.SimpleCircuitBreaker;
import org.tango.utils.TangoUtil;

import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.Tango.DispLevel;
import fr.soleil.tango.clientapi.TangoAttribute;
import fr.soleil.tango.server.attributelistwriter.dataaccess.AsyncGroupReader;
import fr.soleil.tango.server.attributelistwriter.dataaccess.AuditTrailer;
import fr.soleil.tango.server.attributelistwriter.dataaccess.IAuditListener;
import fr.soleil.tango.server.attributelistwriter.dynamic.ListValuesAttribute;
import fr.soleil.tango.server.attributelistwriter.dynamic.ListWriterAttribute;
import fr.soleil.tango.server.attributelistwriter.dynamic.ListWriterBuilder;
import fr.soleil.tango.statecomposer.StateResolver;

@Device(transactionType = TransactionType.NONE)
public final class AttributeListWriter implements IAuditListener {

	private static final int TIMEOUT = 3000;
	private static final int STATE_REFRESH_PERIOD = 1000;
	private static final String CONFIG_ERROR = "CONFIG_ERROR";
	private static final String STOP_COMMAND_NAME = "StopAll";
	private static final String UNKNOWN = "UNKNOWN";
	private final Logger logger = LoggerFactory.getLogger(AttributeListWriter.class);
	private final XLogger xlogger = XLoggerFactory.getXLogger(AttributeListWriter.class);

	/**
	 * The number version of the device
	 */
	@Attribute(displayLevel = DispLevel._EXPERT)
	private static String version = "";

	/**
	 * MAIN
	 */
	public static void main(final String[] args) {
		final ResourceBundle rb = ResourceBundle.getBundle("fr.soleil.attributelistwriter.application");
		version = rb.getString("project.version");
		ServerManager.getInstance().start(args, AttributeListWriter.class);
	}

	/**
	 * list of attributes to write with the value before any write on an attribute
	 * group.
	 */
	@DeviceProperty
	private String[] preWritingList = new String[0];

	/**
	 * list of attributes to write with the value after any write on an attribute
	 * group.
	 */
	@DeviceProperty
	private String[] postWritingList = new String[0];

	/**
	 * list of attributes to manage
	 */
	@DeviceProperty
	private String[] attributeNameList = new String[0];

	/**
	 * list of dynamics commands
	 */
	@DeviceProperty
	private String[] commandNameList = new String[0];

	/**
	 * The list of priorities for state composer
	 */
	@DeviceProperty
	private String[] statePriorities = new String[0];

	@DeviceProperty
	private String[] accuracyList = new String[0];
	/**
	 * The list of commands to stop all underlying devices
	 */
	@DeviceProperty
	private String[] stopCommandFullNameList;

	/**
	 * Condition of state to start device
	 */
	@DeviceProperty
	private String stateCondition = "";

	private StateResolver stateReader;

	/**
	 * boolean to know if they devices states are in state defined by property
	 * StateCondition
	 */
	@Attribute(displayLevel = DispLevel._EXPERT)
	private boolean attributesArrived;

	/**
	 * The last executed command name
	 */
	@Attribute
	private volatile String executedCommand = UNKNOWN;

	/**
	 * The time stamp of the execution
	 */
	@Attribute
	private volatile String executedTimeStamp = UNKNOWN;

	@Attribute
	private volatile String lastError = "";

	/**
	 * The state of the device
	 */
	@State
	private DeviceState state = DeviceState.STANDBY;

	/**
	 * The list of attributes
	 */
	private final List<String> attributeList = new ArrayList<String>();

	private final List<Triple<String, String, Double>> preWritingTripleList = new ArrayList<>();

	private final List<Triple<String, String, Double>> postWritingTripleList = new ArrayList<>();

	@DynamicManagement
	private DynamicManager dynMngt;

	@Status
	private volatile String status = "Device is ready";

	@DeviceManagement
	private DeviceManager device;

	private DeviceState arrivingState;
	private boolean checkArrivingState;

	private volatile String invocationContext = "";

	private final List<String> commandLabelNameList = new ArrayList<String>();
	private AsyncGroupReader reader;
	private final Map<String, Short> enumShortMap = new LinkedHashMap<String, Short>();

	/**
	 * circuit breaker pattern for initializing the device
	 *
	 * @author ABEILLE
	 *
	 */
	private class InitCommand implements CircuitBreakerCommand {

		@Override
		public void execute() throws DevFailed {
			logger.info("trying init");
			status = "trying to init";
			checkAttributeCreation();
			createDynamics();
			checkArrivingState = false;
			if (!stateCondition.isEmpty()) {
				arrivingState = DeviceState.valueOf(stateCondition);
				logger.debug("check attributes arrived on {}", DeviceState.toDevState(stateCondition));
				checkArrivingState = true;
			}
			stateReader = new StateResolver(STATE_REFRESH_PERIOD, false);
			stateReader.configurePriorities(statePriorities);
			// retrieve device name from attribute name
			final Set<String> deviceNameList = new HashSet<String>();
			for (final String element : attributeNameList) {
				final String deviceName = TangoUtil.getfullDeviceNameForAttribute(element);
				deviceNameList.add(deviceName);
			}
			for (final String element : preWritingList) {
				// Still works even if there is a value in the string.
				final String deviceName = TangoUtil.getfullDeviceNameForAttribute(element);
				deviceNameList.add(deviceName);
			}
			for (final String element : postWritingList) {
				// Still works even if there is a value in the string.
				final String deviceName = TangoUtil.getfullDeviceNameForAttribute(element);
				deviceNameList.add(deviceName);
			}
			stateReader.setMonitoredDevices(TIMEOUT, deviceNameList.toArray(new String[deviceNameList.size()]));
			stateReader.start(device.getName());
		}

		@Override
		public void getFallback() throws DevFailed {
			partialDelete();
			// dynMngt.clearAttributesWithExclude("log"); //It doesn't work, a workaround is used until debug.
			clearAttributesWithExclude("log");
		}

		@Override
		public void notifyError(final DevFailed e) {
			final StringBuilder sb = new StringBuilder();
			sb.append("INIT FAILED, will retry in a while\n").append(DevFailedUtils.toString(e));
			status = sb.toString();
			logger.error("{}", status);
		}
	}
	
	
    private void clearAttributesWithExclude(final String... exclude) throws DevFailed {
        final String[] toExclude = new String[exclude.length];
        for (int i = 0; i < toExclude.length; i++) {
            toExclude[i] = exclude[i].toLowerCase(Locale.ENGLISH);
        }
        final List<String> attributes =  new ArrayList<>(dynMngt.getDynamicAttributes().size());
        dynMngt.getDynamicAttributes().forEach(attr -> {
			try {
				attributes.add(attr.getConfiguration().getName());
			} catch (DevFailed e) {
				logger.error("Attribute removal error.", e);
			}
		});
        for (final String attributeName : attributes) {
            if (!ArrayUtils.contains(toExclude, attributeName)) {
            	dynMngt.removeAttribute(attributeName);
            }
        }
    }


	/**
	 * check if the attributs are scalar, writable and create TangoAttribute
	 *
	 * @throws DevFailed
	 */
	private void checkAttributeCreation() throws DevFailed {
		xlogger.entry();
		if (attributeNameList.length == 0 || attributeNameList[0].trim().equals("")) {
			status = "No attribute defined in AttributeNameList property";
			logger.debug("No attribute defined in AttributeNameList property");
			xlogger.exit();
			throw DevFailedUtils.newDevFailed(CONFIG_ERROR, status);
		}

		// set to remove duplications, LinkedHashSet to keep order
		final Set<String> set = new LinkedHashSet<String>(Arrays.asList(attributeNameList));
		for (final String element : set) {
			final String attrName = element.trim();
			final TangoAttribute attr = new TangoAttribute(attrName);
			if (!attr.isScalar()) {
				xlogger.exit();
				throw DevFailedUtils.newDevFailed(CONFIG_ERROR, attrName + " must be a scalar");
			}

			if (attr.getWriteType().equals(AttrWriteType.READ)) {
				logger.debug(status);
				xlogger.exit();
				throw DevFailedUtils.newDevFailed(CONFIG_ERROR, attrName + " must be a writable");
			}
			attributeList.add(attrName);
			// attributeTable.add(attr);
		}

		// check for pre and post writing
		for (final String element : preWritingList) {
			Triple<String, String, Double> triple = treatPrePostWritingElement(element);
			preWritingTripleList.add(triple);
		}

		for (final String element : postWritingList) {
			Triple<String, String, Double> triple = treatPrePostWritingElement(element);
			postWritingTripleList.add(triple);
		}

		xlogger.exit();
	}

	private Triple<String, String, Double> treatPrePostWritingElement(String element) throws DevFailed {
		xlogger.entry();
		final StringTokenizer tokens = new StringTokenizer(element, ";");
		final int nbTokens = tokens.countTokens();
		if (nbTokens < 2 || nbTokens > 3) {
			final String errorMsg = "Wrong definition for postWrittingList property, it must be : \n attribute ; value ; accuracy"
					+ "\nThe accuracy is optionnal. Default value is 0. It's unused in case of types such as boolean and strings."
					+ "\nError detected for the folowing line: " + element;
			xlogger.exit();
			throw DevFailedUtils.newDevFailed(CONFIG_ERROR, errorMsg);
		}
		String attrName = tokens.nextToken().trim().toLowerCase();

		final TangoAttribute attr = new TangoAttribute(attrName);
		if (!attr.isScalar()) {
			xlogger.exit();
			throw DevFailedUtils.newDevFailed(CONFIG_ERROR, attrName + " must be a scalar");
		}

		if (attr.getWriteType().equals(AttrWriteType.READ)) {
			logger.debug(status);
			xlogger.exit();
			throw DevFailedUtils.newDevFailed(CONFIG_ERROR, attrName + " must be a writable");
		}
		String value = tokens.nextToken().trim();
		double accuracy = 0.0;
		if (nbTokens == 3) {
			String accuracyStr = tokens.nextToken().trim();
			try {
				accuracy = Double.valueOf(accuracyStr);
			} catch (NumberFormatException nfe) {
				throw DevFailedUtils.newDevFailed(nfe);
			}
		}
		xlogger.exit();
		return new ImmutableTriple<>(attrName, value, accuracy);
	}

	private class Config {
		private final String name;
		private final List<Triple<String, String, Double>> config;

		public Config(final String name, final List<String> attributeNameList, final List<String> attributeValueList,
				final List<Double> accuracyList) {
			super();
			this.name = name;
			config = new ArrayList<Triple<String, String, Double>>();
			for (int i = 0; i < attributeNameList.size(); i++) {
				final Triple<String, String, Double> triple = new ImmutableTriple<String, String, Double>(
						attributeNameList.get(i).toLowerCase(), attributeValueList.get(i), accuracyList.get(i));
				config.add(triple);
			}

		}

		public String getName() {
			return name;
		}

		public List<Triple<String, String, Double>> getConfig() {
			return config;
		}
	}

	private final Set<String> copyArrayAndClean(final String[] array) {
		final Set<String> tmp = new LinkedHashSet<String>(Arrays.asList(array));
		tmp.removeAll(Arrays.asList("", null));
		final Set<String> result = new LinkedHashSet<String>();
		for (final String element : tmp) {
			final String trimmed = element.trim();
			if (!trimmed.isEmpty()) {
				result.add(trimmed);
			}
		}
		return result;
	}

	@Init(lazyLoading = true)
	public void initDevice() throws DevFailed {
		xlogger.entry();
		// create log attribute
		dynMngt.addAttribute(new LogAttribute(10, LoggerFactory.getLogger(AuditTrailer.class)));
		// try init until OK
		new SimpleCircuitBreaker(new InitCommand()).execute();
		logger.info("init OK");
		xlogger.exit();
	}

	@Delete
	public void delete() throws DevFailed {
		partialDelete();
		dynMngt.clearAll();
	}

	/**
	 * clear to retry init
	 *
	 * @throws DevFailed
	 */
	private void partialDelete() throws DevFailed {
		attributeList.clear();
		commandLabelNameList.clear();
		lastError = "";
		executedCommand = "";
		executedTimeStamp = "";
		preWritingTripleList.clear();
		postWritingTripleList.clear();
		if (stateReader != null) {
			stateReader.stop();
		}
	}

	/**
	 * Create the attributes and the commands
	 *
	 * @throws DevFailed
	 */
	private void createDynamics() throws DevFailed {
		xlogger.entry();

		final List<Double> accuracyColl = new ArrayList<Double>();
		if (accuracyList.length == 1 && !accuracyList[0].isEmpty()) {
			for (int j = 0; j < attributeNameList.length; j++) {
				accuracyColl.add(Double.valueOf(accuracyList[0]));
			}
		} else if (accuracyList.length == 1 && accuracyList[0].isEmpty() || accuracyList.length == 0) {
			for (int j = 0; j < attributeNameList.length; j++) {
				accuracyColl.add(0.0);
			}
		} else if (accuracyList.length == attributeNameList.length) {
			for (int j = 0; j < attributeNameList.length; j++) {
				accuracyColl.add(Double.valueOf(accuracyList[j]));
			}
		} else {
			throw DevFailedUtils
					.newDevFailed("accuracy size must be empty, have 1 element or " + attributeNameList.length);
		}

		// create a "StopAll" command
		final Set<String> stopCmdList = copyArrayAndClean(stopCommandFullNameList);
		if (!stopCmdList.isEmpty()) {
			final AsyncGroupCommand behavior = new AsyncGroupCommand(STOP_COMMAND_NAME,
					stopCmdList.toArray(new String[stopCmdList.size()]));
			dynMngt.addCommand(behavior);

		}

		// create dynamic commands and attributes
		final Set<String> cmdList = copyArrayAndClean(commandNameList);
		System.out.println("cmdList " + cmdList);
		if (cmdList.isEmpty()) {
			xlogger.exit();
			throw DevFailedUtils.newDevFailed(CONFIG_ERROR, "No command defined in CommandNameList property");
		} else {
			final List<Config> configList = new ArrayList<Config>();
			for (final String element : cmdList) {
				// Now all the information is in the commandNameList property
				final StringTokenizer tokens = new StringTokenizer(element, ";");
				final int nbTokens = tokens.countTokens();
				if (nbTokens - 1 != attributeNameList.length) {
					final String errorMsg = "Wrong definition for commandNameList property, it must be : \n commandName ; value1; value2; value3"
							+ "\nAnd the number of defined value must be equal to the AttributeNameList property size"
							+ "\nError detected for the folowing line: " + element;
					xlogger.exit();
					throw DevFailedUtils.newDevFailed(CONFIG_ERROR, errorMsg);
				}

				String commandLabel = tokens.nextToken().trim();
				commandLabel = commandLabel.replaceAll(" ", "_");
				commandLabelNameList.add(commandLabel);
				final String attributeLabel = "config" + commandLabel;
				final List<String> valueList = new ArrayList<String>();
				// final int count = 0;
				while (tokens.hasMoreTokens()) {
					valueList.add(tokens.nextToken());
					// valueList[count++] = tokens.nextToken();
				}
				final String[] attrValue = new String[attributeNameList.length];
				for (int j = 0; j < attributeNameList.length; j++) {
					attrValue[j] = attributeNameList[j] + " = " + valueList.get(j) + " +/-" + accuracyColl.get(j);
				}
				logger.debug("building attribute {}", attributeLabel);
				dynMngt.addAttribute(new ListValuesAttribute(attributeLabel, attrValue));

				final Config config = new Config(commandLabel, Arrays.asList(attributeNameList), valueList,
						accuracyColl);
				configList.add(config);

			}
			reader = ListWriterBuilder.buildListReader(this, Arrays.asList(attributeNameList));
			short i = 0;
			for (final Config config : configList) {
				logger.debug("building command and attribute {}", config.getName());
				ListWriterBuilder.buildListWriter(this, config.getName(), config.getConfig(), preWritingTripleList,
						postWritingTripleList, dynMngt, reader);
				enumShortMap.put(config.getName(), i);
				i++;
			}
			xlogger.exit();
		}
	}

	/**
	 * Called before command execution to update its name and its timestamp
	 *
	 * @param ctx
	 */
	@AroundInvoke
	public void aroundInvoke(final InvocationContext ctx) {
		if (ctx.getContext().equals(ContextType.PRE_COMMAND)
				|| ctx.getContext().equals(ContextType.PRE_WRITE_ATTRIBUTE)) {
			final String cmdName = ctx.getNames()[0];
			if (!cmdName.equals("State") && !cmdName.equals("Status") && !cmdName.equals("Init")) {
				final StringBuilder sb = new StringBuilder();
				invocationContext = sb.append(cmdName).append(" from ")
						.append(ServerRequestInterceptor.getInstance().getClientHostName()).append(" ")
						.append(ClientIDUtil.toString(ctx.getClientID())).toString();
				LoggerFactory.getLogger(AuditTrailer.class).info(invocationContext);
			}
		}
	}

	@Attribute(name = "CurrentPositions")
	public String[] getCurrentPositions() {
		final Map<String, String> values = reader.getCurrentValues();
		final String[] result = new String[values.size()];
		int i = 0;
		for (final Entry<String, String> entry : values.entrySet()) {
			result[i++] = entry.getKey() + " = " + entry.getValue();
		}
		return result;
	}

	@Attribute(name = "CurrentPositionValues", displayLevel = DispLevel._EXPERT)
	public String[] getCurrentPositionValues() {
		final Map<String, String> values = reader.getCurrentValues();
		final String[] result = new String[values.size()];
		int i = 0;
		for (final Entry<String, String> entry : values.entrySet()) {
			result[i++] = entry.getValue();
		}
		return result;
	}

	/**
	 * To read state
	 *
	 * @return
	 */
	public final DeviceState getState() {
		xlogger.entry();
		if (checkArrivingState) {
			try {
				if (isAttributesArrived()) {
					state = arrivingState;
				} else {
					state = DeviceState.getDeviceState(stateReader.getState());
				}
			} catch (final DevFailed e) {
				// error getting state
				state = DeviceState.UNKNOWN;
			}
		} else {
			state = DeviceState.getDeviceState(stateReader.getState());
		}
		xlogger.exit();
		return state;
	}

	/**
	 * To read status
	 *
	 * @return
	 */
	public final String getStatus() {
		xlogger.entry();
		final StringBuilder sb = new StringBuilder();
		sb.append("Last error: ").append(lastError).append("\n");
		if (checkArrivingState) {
			try {
				if (isAttributesArrived()) {
					sb.append("all attributes arrived\n");
				} else {
					sb.append("attributes moving\n");
				}
			} catch (final DevFailed e) {
				sb.append("error:\n").append(DevFailedUtils.toString(e));
			}
		}
		if (stateReader != null) {
			final String[] st = stateReader.getDeviceStateArray();
			sb.append("At least one device is ").append(DeviceState.getDeviceState(stateReader.getState()))
					.append("\n");
			for (final String element : st) {
				sb.append(element).append("\n");
			}
			sb.append("Last execution: ").append(invocationContext).append("\n");
			status = sb.toString();
		}
		xlogger.exit();
		return status;
	}

	@Attribute(name = "HardwareStates")
	public DevState[] getHardwareStates() {
		final Collection<DevState> states = stateReader.getDeviceStates().values();
		return states.toArray(new DevState[states.size()]);
	}

	public final String getLastError() {
		return lastError;
	}

	@Command(name = "GetCommandNameList")
	public String[] getCommandNameList() {
		return commandLabelNameList.toArray(new String[commandLabelNameList.size()]);
	}

	@Attribute(name = "EnumShortValue", displayLevel = DispLevel._EXPERT)
	public short getEnumShortValue() throws DevFailed {
		short enumShortValue = Short.MIN_VALUE;
		for (final IAttributeBehavior attr : dynMngt.getDynamicAttributes()) {
			if (attr instanceof ListWriterAttribute) {
				final ListWriterAttribute listWriterAttribute = (ListWriterAttribute) attr;
				if (listWriterAttribute.isActive()) {
					enumShortValue = enumShortMap.get(listWriterAttribute.getName());
					break;
				}
			}
		}
		return enumShortValue;
	}

	public void setEnumShortValue(final short value) throws DevFailed {
		for (final IAttributeBehavior attr : dynMngt.getDynamicAttributes()) {
			if (attr instanceof ListWriterAttribute) {
				final ListWriterAttribute listWriterAttribute = (ListWriterAttribute) attr;
				if (enumShortMap.get(listWriterAttribute.getName()) == value) {
					listWriterAttribute.execute();
					break;
				}
			}
		}
	}

	/**
	 * return the selected attribute
	 *
	 * @return selected attribute
	 * @throws DevFailed
	 */
	@Attribute
	public String getSelectedAttributeName() throws DevFailed {
		xlogger.entry();
		String selectedAttributeName = UNKNOWN;
		for (final IAttributeBehavior attr : dynMngt.getDynamicAttributes()) {
			if (attr instanceof ListWriterAttribute) {
				final ListWriterAttribute listWriterAttribute = (ListWriterAttribute) attr;
				if (listWriterAttribute.isActive()) {
					selectedAttributeName = listWriterAttribute.getName();
					break;
				}
			}
		}
		xlogger.exit();
		return selectedAttributeName;
	}

	/**
	 * To know if the devices used are in the same state of property StateCondition
	 *
	 * @return
	 * @throws DevFailed
	 */
	public final boolean isAttributesArrived() throws DevFailed {
		attributesArrived = true;
		if (checkArrivingState) {
			final Collection<DevState> states = stateReader.getDeviceStates().values();
			for (final DevState s : states) {
				if (!s.equals(arrivingState.getDevState())) {
					attributesArrived = false;
					break;
				}
			}
		}
		return attributesArrived;
	}

	public final String getExecutedTimeStamp() {
		return executedTimeStamp;
	}

	public final String getVersion() {
		return version;
	}

	public void setState(final DeviceState state) {
		this.state = state;
	}

	public void setStatus(final String status) {
		this.status = status;
	}

	public void setDynMngt(final DynamicManager dynMngt) {
		this.dynMngt = dynMngt;
	}

	public void setPreWritingList(final String[] preWritingList) {
		this.preWritingList = Arrays.copyOf(preWritingList, preWritingList.length);
	}

	public void setPostWritingList(final String[] postWritingList) {
		this.postWritingList = Arrays.copyOf(postWritingList, postWritingList.length);
	}

	public void setAttributeNameList(final String[] attributeNameList) {
		this.attributeNameList = Arrays.copyOf(attributeNameList, attributeNameList.length);
	}

	public void setCommandNameList(final String[] commandNameList) {
		this.commandNameList = Arrays.copyOf(commandNameList, commandNameList.length);
	}

	public void setStatePriorities(final String[] statePriorities) {
		this.statePriorities = Arrays.copyOf(statePriorities, statePriorities.length);
	}

	public void setAccuracyList(final String[] accuracyList) {
		this.accuracyList = Arrays.copyOf(accuracyList, accuracyList.length);
	}

	public void setStateCondition(final String stateCondition) {
		this.stateCondition = stateCondition;
	}

	public final String getExecutedCommand() {
		return executedCommand;
	}

	public void setDevice(final DeviceManager device) {
		this.device = device;
	}

	@Override
	public void setLastExecutedTimestamp(final String timestamp) {
		executedTimeStamp = timestamp;

	}

	@Override
	public void setLastExecutedEvent(final String eventName) {
		executedCommand = eventName;
	}

	@Override
	public void setLastError(final String errorMsg) {
		lastError = errorMsg;
	}

	public void setStopCommandFullNameList(final String[] stopCommandFullNameList) {
		this.stopCommandFullNameList = stopCommandFullNameList;
	}

}